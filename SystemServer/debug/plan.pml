<?xml version="1.0" encoding="GB2312"?>
<!-- edited with XML Spy v4.4 U (http://www.xmlspy.com) by deng-cz (agree) -->
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1
D:\geronimo-1.1\schema\geronimo-module-1.1.xsd">
	<environment>
		<moduleId>
			<groupId>SystemServer</groupId>
			<artifactId>debug</artifactId>
			<version>1.0</version>
		</moduleId>
		<dependencies>
			<dependency>
				<groupId>SystemServer</groupId>
				<artifactId>base</artifactId>
				<version>1.0</version>
				<type>car</type>
			</dependency>
		</dependencies>
	</environment>
	<gbean class="cn.com.agree.eai.debug.ApplicationMonitor" name="applicationMonitor">
		<references name="Applications">
			<pattern>
				<type>GBean</type>
				<name>clientDispatcher</name>
			</pattern>
		</references>
		<references name="ServerApplications">
			<pattern>
				<type>GBean</type>
				<name>serverMessageDispatcher</name>
			</pattern>
			<pattern>
				<type>GBean</type>
				<name>appServiceDispather</name>
			</pattern>
		</references>
	</gbean>
	<gbean class="cn.com.agree.eai.debug.DebugServer" name="listener">
		<attribute name="commandListenPort">22223</attribute>
		<attribute name="eventListenPort">22224</attribute>
		<reference name="ApplicationMonitor">
			<type>GBean</type>
			<name>applicationMonitor</name>
		</reference>
	</gbean>
</module>
