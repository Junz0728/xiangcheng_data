<?xml version="1.0" encoding="GBK"?>
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1 D:\geronimo-1.1\schema\geronimo-module-1.1.xsd">
    <environment>
        <moduleId>
            <groupId>SystemServer</groupId>
            <artifactId>connector</artifactId>
            <version>1.0</version>
        </moduleId>
        <dependencies>
            <dependency>
                <groupId>SystemServer</groupId>
                <artifactId>base</artifactId>
                <version>1.0</version>
                <type>car</type>
            </dependency>
            <dependency>
                <groupId>SystemServer</groupId>
                <artifactId>activemq</artifactId>
                <version>1.0</version>
                <type>car</type>
            </dependency>
        </dependencies>
    </environment>
    <gbean class="cn.com.agree.eai.core.jms.ConnectionFactoryGBean" name="connectionFactory">
        <attribute name="brokerURL" type="java.lang.String">vm://broker?broker.persistent=false</attribute>
    </gbean>
</module>
