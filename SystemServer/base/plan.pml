<?xml version="1.0" encoding="GBK"?>
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1">
    <environment>
        <moduleId>
            <groupId>SystemServer</groupId>
            <artifactId>base</artifactId>
            <version>1.0</version>
        </moduleId>
        <dependencies>
            <dependency>
                <groupId>org.apache.geronimo.framework</groupId>
                <artifactId>j2ee-system</artifactId>
                <version>2.2</version>
                <type>car</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>afe</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>abrm</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>component</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>tools</artifactId>
                <version>1.0.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>configServer</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>abpmDao</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>bcprov-jdk14</artifactId>
                <version>133</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.geronimo.specs</groupId>
                <artifactId>geronimo-jms_1.1_spec</artifactId>
                <version>1.1.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jetty</groupId>
                <artifactId>jetty</artifactId>
                <version>6.1.22</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jetty</groupId>
                <artifactId>jetty-util</artifactId>
                <version>6.1.22</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis</groupId>
                <artifactId>axis</artifactId>
                <version>1.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis</groupId>
                <artifactId>axis-ant</artifactId>
                <version>1.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-discovery</artifactId>
                <version>0.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis</groupId>
                <artifactId>jaxrpc</artifactId>
                <version>1.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis</groupId>
                <artifactId>saaj</artifactId>
                <version>1.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>wsdl4j</artifactId>
                <version>1.6.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-dbcp</artifactId>
                <version>1.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-pool</artifactId>
                <version>1.5.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-net</artifactId>
                <version>3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.2.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-codec</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>1.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>ognl</groupId>
                <artifactId>ognl</artifactId>
                <version>2.6.9</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>springframework</groupId>
                <artifactId>spring</artifactId>
                <version>2.5.6</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>concurrent</groupId>
                <artifactId>concurrent</artifactId>
                <version>1.3.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>dom4j</groupId>
                <artifactId>dom4j</artifactId>
                <version>1.6.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>hibernate</groupId>
                <artifactId>hibernate</artifactId>
                <version>3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>5.1.6-bin</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>oracle</groupId>
                <artifactId>ojdbc</artifactId>
                <version>14</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>db</groupId>
                <artifactId>sqljdbc4</artifactId>
                <version>2.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.codehaus.groovy</groupId>
                <artifactId>groovy-all</artifactId>
                <version>1.8.7</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bsh</groupId>
                <artifactId>bsh</artifactId>
                <version>1.3.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>ehcache</groupId>
                <artifactId>ehcache-core</artifactId>
                <version>2.3.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jta</groupId>
                <artifactId>jta</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>antlr</groupId>
                <artifactId>antlr</artifactId>
                <version>2.7.7</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>activemq</groupId>
                <artifactId>activemq-all</artifactId>
                <version>5.3.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>activemq</groupId>
                <artifactId>xbean-spring</artifactId>
                <version>3.6</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>activemq</groupId>
                <artifactId>org_osgi_core</artifactId>
                <version>4.1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.xbean</groupId>
                <artifactId>xbean-naming</artifactId>
                <version>3.6</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jbpm</groupId>
                <artifactId>jbpm</artifactId>
                <version>3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>mq</groupId>
                <artifactId>connector</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>mq</groupId>
                <artifactId>com.ibm.mq</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>mq</groupId>
                <artifactId>com.ibm.mqjms</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>mq</groupId>
                <artifactId>dhbcore</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jakarta</groupId>
                <artifactId>jakarta-oro</artifactId>
                <version>2.0.8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor-xml</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor-core</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jt400</groupId>
                <artifactId>jt400</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axiom</groupId>
                <artifactId>axiom-api</artifactId>
                <version>1.2.8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axiom</groupId>
                <artifactId>axiom-dom</artifactId>
                <version>1.2.8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axiom</groupId>
                <artifactId>axiom-impl</artifactId>
                <version>1.2.8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons-httpclient</groupId>
                <artifactId>commons-httpclient</artifactId>
                <version>3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons-httpclient</groupId>
                <artifactId>commons-httpclient-contrib</artifactId>
                <version>3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis2</groupId>
                <artifactId>axis2-kernel</artifactId>
                <version>1.4.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>axis2</groupId>
                <artifactId>axis2-adb</artifactId>
                <version>1.4.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>XmlSchema</groupId>
                <artifactId>XmlSchema</artifactId>
                <version>1.3.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>httpcore</groupId>
                <artifactId>httpcore</artifactId>
                <version>4.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>tuxedo</groupId>
                <artifactId>jolt</artifactId>
                <version>1.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>cics</groupId>
                <artifactId>ctgclient</artifactId>
                <version>7.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>cics</groupId>
                <artifactId>ctgserver</artifactId>
                <version>7.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>adept</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-io</artifactId>
                <version>1.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>elang</groupId>
                <artifactId>rabbitmq-client</artifactId>
                <version>1.5.5</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>elang</groupId>
                <artifactId>retrotranslator-runtime</artifactId>
                <version>1.2.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.geronimo.specs</groupId>
                <artifactId>geronimo-servlet_2.5_spec</artifactId>
                <version>1.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>com.jcraft</groupId>
                <artifactId>jsch</artifactId>
                <version>0.1.42</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.codehaus</groupId>
                <artifactId>mvel2</artifactId>
                <version>2.0.16</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.xerces</groupId>
                <artifactId>xercesImpl</artifactId>
                <version>2.9.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jasper</groupId>
                <artifactId>jasper-compiler</artifactId>
                <version>5.5.15</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jasper</groupId>
                <artifactId>jasper-compiler-jdt</artifactId>
                <version>5.5.15</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jasper</groupId>
                <artifactId>jasper-runtime</artifactId>
                <version>5.5.15</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jsp</groupId>
                <artifactId>jsp-api</artifactId>
                <version>2.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>neethi</artifactId>
                <version>2.0.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>backport-util-concurrent</artifactId>
                <version>3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>woden-impl-dom</artifactId>
                <version>1.0M8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>woden-api</artifactId>
                <version>1.0M8</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>bpm</groupId>
                <artifactId>annogen</artifactId>
                <version>0.1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-el</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-lang</artifactId>
                <version>2.5</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>hibernate</groupId>
                <artifactId>javassist</artifactId>
                <version>3.12.0.GA</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>hibernate</groupId>
                <artifactId>proxool</artifactId>
                <version>0.9.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>hibernate</groupId>
                <artifactId>proxool</artifactId>
                <version>cglib</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>db.db2</groupId>
                <artifactId>db2jdbc4</artifactId>
                <version>9.7</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>db.db2</groupId>
                <artifactId>db2jdbclic</artifactId>
                <version>9.7</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>db</groupId>
                <artifactId>sybase</artifactId>
                <version>6.05</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>ant</groupId>
                <artifactId>ant</artifactId>
                <version>1.7.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>javabase64</groupId>
                <artifactId>javabase64</artifactId>
                <version>1.3.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.json</groupId>
                <artifactId>json</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
        </dependencies>
    </environment>
    <gbean class="cn.com.agree.eai.config.BaseServer" name="baseServer">
        <attribute name="interval" type="int">60000</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.jms.impl.AfeCommand" name="afeJmxServer"/>
    <gbean class="cn.com.agree.eai.core.registry.BasicServiceRegistry" name="basicServiceRegistry"/>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="bcspdb2_plat">
        <attribute name="username" type="java.lang.String">bcsp</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;YmNzcA==</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.ibm.db2.jcc.DB2Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:db2://182.50.210.6:50001/dbbcsp</attribute>
        <attribute name="maxActive" type="int">100</attribute>
        <attribute name="minActive" type="int">10</attribute>
        <attribute name="maxIdle" type="int">20</attribute>
        <attribute name="minIdle" type="int">10</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select * from sysibm.sysdummy1</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="bbipdb2_plat">
        <attribute name="username" type="java.lang.String">bbipadm</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;YmJpcGFkbQ==</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.ibm.db2.jcc.DB2Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:db2://182.50.213.56:60013/bbipdb</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">2</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select * from sysibm.sysdummy1</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="sybase_plat">
        <attribute name="username" type="java.lang.String">plat</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;MTIzNDU2</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.sybase.jdbc3.jdbc.SybDriver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:sybase:Tds:182.50.5.90:8888/platdb?charset=cp936&amp;SelectMethod=Cursor</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">10000</attribute>
        <attribute name="validationQuery" type="java.lang.String">set rowcount 1 select name from sysobjects where type='U'</attribute>
    </gbean>
</module>
